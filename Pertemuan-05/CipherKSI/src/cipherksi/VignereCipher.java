/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package cipherksi;

/**
 *
 * @author GHOLIDHO
 */
import java.util.Scanner;

public class VignereCipher {
    // Fungsi ini menghasilkan kunci yang sesuai panjang dengan teks yang akan dienkripsi.
    // Jika panjang kunci kurang dari panjang teks, fungsi akan mengulang kunci dari awal hingga mencapai panjang yang sesuai.
    static String generateKey(String str, String key) {
        int panjangTeks = str.length();

        for (int i = 0;; i++) {
            if (panjangTeks == i)
                i = 0;
            if (key.length() == panjangTeks)
                break;
            key += (key.charAt(i));
        }
        return key;
    }

    // Fungsi ini digunakan untuk mengenkripsi teks menggunakan kunci Vigenere Cipher.
    // Setiap karakter dalam teks diubah menjadi karakter terenkripsi dengan menggunakan aturan Vigenere Cipher.
    // Proses ini melibatkan penjumlahan modulo 26 antara karakter teks asli dan karakter kunci.
    static String cipherText(String str, String key) {
        StringBuilder cipherText = new StringBuilder();

        for (int i = 0, j = 0; i < str.length(); i++) {
            char karakter = str.charAt(i);

            if (Character.isLetter(karakter)) {
                // Konversi ke dalam rentang 0-25
                int x = (karakter + key.charAt(j)) % 26;

                // Konversi menjadi huruf alfabet (ASCII)
                x += 'A';

                cipherText.append((char) (x));
                j = (j + 1) % key.length();
            } else {
                // Menambahkan karakter non-huruf tanpa mengubahnya
                cipherText.append(karakter);
            }
        }
        return cipherText.toString();
    }

    // Fungsi ini digunakan untuk mendekripsi teks yang telah dienkripsi menggunakan Vigenere Cipher.
    // Setiap karakter dalam teks terenkripsi diubah kembali menjadi karakter asli dengan menggunakan aturan Vigenere Cipher.
    // Proses ini melibatkan pengurangan modulo 26 antara karakter teks terenkripsi dan karakter kunci.
    static String originalText(String cipherText, String key) {
        StringBuilder originalText = new StringBuilder();

        for (int i = 0, j = 0; i < cipherText.length(); i++) {
            char karakter = cipherText.charAt(i);

            if (Character.isLetter(karakter)) {
                // Konversi ke dalam rentang 0-25
                int x = (karakter - key.charAt(j) + 26) % 26;

                // Konversi menjadi huruf alfabet (ASCII)
                x += 'A';
                originalText.append((char) (x));
                j = (j + 1) % key.length();
            } else {
                // Menambahkan karakter non-huruf tanpa mengubahnya
                originalText.append(karakter);
            }
        }
        return originalText.toString();
    }

    // Fungsi ini akan mengonversi karakter-karakter huruf kecil dalam sebuah string menjadi huruf besar.
    // Ini diperlukan karena Vigenere Cipher hanya berlaku untuk huruf besar.
    static String konversiHurufKecilKeHurufBesar(String s) {
        StringBuilder str = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLowerCase(s.charAt(i))) {
                str.setCharAt(i, Character.toUpperCase(s.charAt(i)));
            }
        }
        return str.toString();
    }

    // Program Utama
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Teks: ");
        String teks = input.nextLine();

        System.out.print("Kunci: ");
        String kunci = input.nextLine();

        // Mengonversi teks dan kunci menjadi huruf besar
        String str = konversiHurufKecilKeHurufBesar(teks);
        String keyword = konversiHurufKecilKeHurufBesar(kunci);

        // Membuat kunci yang sesuai panjang
        String key = generateKey(str, keyword);

        // Melakukan enkripsi teks
        String cipherText = cipherText(str, key);

        System.out.println("Teks Terenkripsi: " + cipherText + "\n");

        // Melakukan dekripsi teks
        System.out.println("Teks Asli/Didekripsi: " + originalText(cipherText, key));
    }
}
