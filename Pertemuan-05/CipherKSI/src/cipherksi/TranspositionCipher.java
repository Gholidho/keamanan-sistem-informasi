
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package cipherksi;

/**
 *
 * @author GHOLIDHO
 */
import java.util.Arrays;
import java.util.Scanner;

public class TranspositionCipher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input teks untuk dienkripsi
        System.out.print("Masukkan teks untuk dienkripsi: ");
        String plaintext = scanner.nextLine().toUpperCase();

        // Input kunci (alfabet unik tanpa spasi)
        System.out.print("Masukkan kunci (alfabet unik tanpa spasi): ");
        String key = scanner.nextLine().toUpperCase().replaceAll("[^A-Z]", "");

        // Enkripsi
        String ciphertext = encrypt(plaintext, key);

        // Menampilkan teks terenkripsi
        System.out.println("Teks Terenkripsi: " + ciphertext);
        
         // Dekripsi
        String decryptedText = decrypt(ciphertext, key);

        // Menampilkan teks terdekripsi
        System.out.println("Teks Terdekripsi: " + decryptedText);
        scanner.close();
    }

    // Metode untuk dekripsi
    private static String decrypt(String ciphertext, String key) {
        int colLength = key.length();
        int rowLength = (int) Math.ceil((double) ciphertext.length() / colLength);
        char[][] matrix = new char[rowLength][colLength];

        //mengubah cipherteks ke bentuk matriks
        int index = 0;
        for (int j = 0; j < rowLength; j++) {
            for (int i = 0; i < colLength; i++) {
                matrix[j][i] = ciphertext.charAt(index++);
            }
        }

        // Menampilkan matriks teks terdekripsi
        System.out.println("Matriks Teks Terdekripsi:");
        printMatrix(matrix);

        return readMatrix(matrix, key).trim(); // Menghapus spasi tambahan yang mungkin terjadi di akhir teks terdekripsi
    }

    // Metode untuk menampilkan matriks
    private static void printMatrix(char[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    // Metode untuk membaca teks terdekripsi dari matriks
        // Metode untuk membaca teks terdekripsi dari matriks
     private static String readMatrix(char[][] matrix, String key) {
         int colLength = key.length();
         int rowLength = matrix.length;

         char[] keyArray = key.toCharArray();
         Arrays.sort(keyArray);

         StringBuilder decryptedText = new StringBuilder();

         // Loop untuk membaca matriks per kolom sesuai urutan abjad kunci
         for (int j = 0; j < colLength; j++) {
             int col = key.indexOf(keyArray[j]);
             for (int i = 0; i < rowLength; i++) {
                 decryptedText.append(matrix[i][col]);
             }
         }

         return decryptedText.toString();
     }


    // Metode untuk enkripsi
    private static String encrypt(String plaintext, String key) {
        int colLength = key.length();
        int rowLength = (int) Math.ceil((double) plaintext.length() / colLength);
        char[][] matrix = new char[rowLength][colLength];

        for (char[] row : matrix) {
            Arrays.fill(row, ' ');
        }

        char[] keyArray = key.toCharArray();
        Arrays.sort(keyArray);

        int index = 0;
        for (int j = 0; j < colLength; j++) {
            int col = key.indexOf(keyArray[j]);
            for (int i = 0; i < rowLength; i++) {
                matrix[i][col] = (index < plaintext.length()) ? plaintext.charAt(index++) : ' ';
            }
        }

        return readMatrix(matrix);
    }

    // Metode untuk membaca teks terenkripsi dari matriks
    private static String readMatrix(char[][] matrix) {
        StringBuilder ciphertext = new StringBuilder();
        for (char[] row : matrix) {
            for (char ch : row) {
                ciphertext.append(ch);
            }
        }
        return ciphertext.toString();
    }
}

